package com.example.leonardoarmero.booker.model.repositories

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import android.os.AsyncTask
import com.example.leonardoarmero.booker.features.application.MainApp
import com.example.leonardoarmero.booker.model.entities.Book
import com.example.leonardoarmero.booker.model.network.BooksAPI
import com.example.leonardoarmero.booker.model.persistence.BooksDatabase
import com.example.leonardoarmero.booker.utils.Constants
import com.example.leonardoarmero.booker.utils.Utils
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject

class BooksRepository(private val application: Application) : BooksResponder {

    // Dependency injection
    @Inject
    lateinit var books: MutableLiveData<Array<Book>>

    private var searchAsyncTask: SearchBooksAsync? = null
    private var db = BooksDatabase.getDatabase(application)

    init {
        MainApp.booksComponent.inject(this)
    }

    fun deleteAllBooks() { DeleteAllBooksAsync(db).execute() }

    fun clearResults() { books.value = emptyArray() }

    fun searchBooks(query: String) {
        if (searchAsyncTask != null) searchAsyncTask!!.cancel(true)

        searchAsyncTask = SearchBooksAsync(db, application, this)
        searchAsyncTask!!.execute(query)
    }

    override fun onBooksReceived(books: Array<Book>?) {
        this.books.value = books
    }

    fun quit() {
        searchAsyncTask?.cancel(true)
        searchAsyncTask = null
    }

    private companion object {
        private fun searchInApi(query: String): Array<Book>? {
            val client = OkHttpClient()
                    .newBuilder()
                    .build()

            val retrofit = Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()

            val booksApi = retrofit.create(BooksAPI::class.java)
            val call = booksApi.getBooks(query)
            val result = call.execute().body()

            return result?.items
        }

        //** ASYNC TASKS **//
        private class InsertBooksAsync(db: BooksDatabase) : AsyncTask<Book, Unit, Unit>() {

            private val mDao = db.bookDao()

            override fun doInBackground(vararg books: Book?) {
                mDao.insert(books)
            }
        }

        private class DeleteAllBooksAsync(db: BooksDatabase) : AsyncTask<Unit, Unit, Unit>() {

            private val mDao = db.bookDao()

            override fun doInBackground(vararg params: Unit?) {
                mDao.deleteAll()
            }
        }

        private class SearchBooksAsync(val db: BooksDatabase, private val application: Application,
                                       val mResponder: BooksResponder)
            : AsyncTask<String, Unit, Array<Book>>() {

            private val mDao = db.bookDao()

            override fun doInBackground(vararg params: String?): Array<Book>? {
                if (params.isEmpty()) return null

                val query = params[0]!!

                val books = if (Utils.isInternetAvailable(application)) {
                    searchInApi(query)
                }
                else {
                    searchInDb(query)
                }

                if (books != null) InsertBooksAsync(db).execute(*books)

                return books
            }

            private fun searchInDb(query: String): Array<Book>? {
                return mDao.getBooks("%$query%")
            }

            override fun onPostExecute(result: Array<Book>?) {
                super.onPostExecute(result)

                if (isCancelled) return

                mResponder.onBooksReceived(result!!)
            }
        }
    }
}