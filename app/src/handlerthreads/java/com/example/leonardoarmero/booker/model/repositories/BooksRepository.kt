package com.example.leonardoarmero.booker.model.repositories

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import android.os.Handler
import com.example.leonardoarmero.booker.features.application.MainApp
import com.example.leonardoarmero.booker.model.entities.Book
import com.example.leonardoarmero.booker.model.network.BooksAPI
import com.example.leonardoarmero.booker.model.persistence.BooksDatabase
import com.example.leonardoarmero.booker.utils.Constants
import com.example.leonardoarmero.booker.utils.Utils
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject

class BooksRepository(private val application: Application) : BooksResponder {

    // Dependency injection
    @Inject
    lateinit var books: MutableLiveData<Array<Book>>

    private val db = BooksDatabase.getDatabase(application)
    // Create an instance of Thread
    private val workerThread = WorkerThread("books_finder")
    // Keep a single instance of handler
    private val uiHandler = Handler()

    private var searchTask: Runnable? = null

    init {
        MainApp.booksComponent.inject(this)
        // Start and prepare
        workerThread.start()
    }

    fun quit() {
        workerThread.quit()
    }

    fun deleteAllBooks() {
        val deleteTask = Runnable {
            db.bookDao().deleteAll()
        }
        workerThread.postTask(deleteTask)
        clearResults()
    }

    fun clearResults() { books.value = emptyArray() }

    fun searchBooks(query: String) {
        workerThread.cancelTask(searchTask)

        searchTask = Runnable {
            // background thread
            val books: Array<Book>?
            if (Utils.isInternetAvailable(application)) {
                books = searchInApi(query)
                insertBooks(books)
            } else {
                books = searchInDb(query, db)
            }

            uiHandler.post({
                // main thread
                onBooksReceived(books)
            })
        }

        // Post single anonymous task
        workerThread.postTask(searchTask!!)
    }

    private fun insertBooks(books: Array<Book>?) {
        if (books != null) db.bookDao().insert(books)
    }

    override fun onBooksReceived(books: Array<Book>?) {
        this.books.value = books
    }

    private companion object {
        private fun searchInApi(query: String): Array<Book>? {
            val client = OkHttpClient()
                    .newBuilder()
                    .build()

            val retrofit = Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()

            val booksApi = retrofit.create(BooksAPI::class.java)
            val call = booksApi.getBooks(query)
            val result = call.execute().body()

            return result?.items
        }

        private fun searchInDb(query: String, db: BooksDatabase): Array<Book>? {
            val mDao = db.bookDao()
            return mDao.getBooks("%$query%")
        }
    }
}