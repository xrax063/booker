package com.example.leonardoarmero.booker.model.repositories

import android.os.Handler
import android.os.HandlerThread
import android.os.Looper
import android.os.Message
import android.util.Log

class WorkerThread(name: String): HandlerThread(name) {

    private lateinit var mWorkerHandler: Handler

    override fun onLooperPrepared() {
        super.onLooperPrepared()
        mWorkerHandler = Handler(looper)
    }

    fun postTask(task: Runnable) {
        mWorkerHandler.post(task)
    }

    fun cancelTask(task: Runnable?) {
        if (task != null) mWorkerHandler.removeCallbacks(task)
    }
}