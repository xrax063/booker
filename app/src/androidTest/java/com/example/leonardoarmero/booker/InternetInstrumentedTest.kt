package com.example.leonardoarmero.booker

import android.content.Context
import android.net.wifi.WifiManager
import android.os.SystemClock
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import com.example.leonardoarmero.booker.utils.Utils

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class InternetInstrumentedTest {

    //** Internet tests **//
    // To test internet availability functions device must be connected
    @Test
    fun testInternetAvailability() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getTargetContext()
        val wifiManager = appContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
        wifiManager.isWifiEnabled = true

        try {
            Thread.sleep(700)
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }

        val isInternetAvailable = Utils.isInternetAvailable(appContext)
        assertTrue(isInternetAvailable)
    }

    @Test
    fun testInternetNotAvailability() {
        val appContext = InstrumentationRegistry.getTargetContext()
        val wifiManager = appContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
        wifiManager.isWifiEnabled = false

        try {
            Thread.sleep(700)
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }

        val isInternetAvailable = Utils.isInternetAvailable(appContext)
        assertFalse(isInternetAvailable)
        wifiManager.isWifiEnabled = true

        try {
            Thread.sleep(700)
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
    }
}
