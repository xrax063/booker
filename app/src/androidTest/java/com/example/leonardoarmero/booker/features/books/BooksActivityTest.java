package com.example.leonardoarmero.booker.features.books;


import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.SystemClock;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.example.leonardoarmero.booker.R;
import com.example.leonardoarmero.booker.utils.Utils;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static junit.framework.Assert.assertFalse;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class BooksActivityTest {

    @Rule
    public ActivityTestRule<BooksActivity> mActivityTestRule =
            new ActivityTestRule<>(BooksActivity.class);

    @Test
    public void searchBookInApiTest() {
        searchBookTest();
    }

    @Test
    public void searchBookInDbTest() {
        Context appContext = InstrumentationRegistry.getTargetContext();
        WifiManager wifiManager = (WifiManager) appContext.getSystemService(Context.WIFI_SERVICE);
        assert wifiManager != null;
        wifiManager.setWifiEnabled(false);

        try {
            Thread.sleep(700);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Boolean isInternetAvailable = Utils.Companion.isInternetAvailable(appContext);
        assertFalse(isInternetAvailable);

        searchBookTest();

        wifiManager.setWifiEnabled(true);

        try {
            Thread.sleep(700);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void searchBookTest() {
        ViewInteraction appCompatImageView = onView(
                allOf(withId(R.id.search_button), withContentDescription("Search"),
                        childAtPosition(
                                allOf(withId(R.id.search_bar),
                                        childAtPosition(
                                                withId(R.id.svBookFinder),
                                                0)),
                                1),
                        isDisplayed()));
        appCompatImageView.perform(click());

        ViewInteraction searchAutoComplete = onView(
                allOf(withId(R.id.search_src_text),
                        childAtPosition(
                                allOf(withId(R.id.search_plate),
                                        childAtPosition(
                                                withId(R.id.search_edit_frame),
                                                1)),
                                0),
                        isDisplayed()));
        searchAutoComplete.perform(replaceText("Android"), closeSoftKeyboard());

        ViewInteraction textView = onView(
                allOf(withId(R.id.tvTitle), withText("Android: Programación de dispositivos " +
                                "móviles a través de ejemplos"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.cvBook),
                                        0),
                                1),
                        isDisplayed()));


        try {
            Thread.sleep(700);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        textView.check(matches(withText("Android: Programación de dispositivos " +
                "móviles a través de ejemplos")));

        ViewInteraction textView2 = onView(
                allOf(withId(R.id.tvAuthor), withText("José Enrique Amaro Soriano"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.cvBook),
                                        0),
                                2),
                        isDisplayed()));

        try {
            Thread.sleep(700);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        textView2.check(matches(withText("José Enrique Amaro Soriano")));
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
