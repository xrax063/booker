package com.example.leonardoarmero.booker.model.persistence

import android.arch.persistence.db.SupportSQLiteDatabase
import android.arch.persistence.room.*
import android.content.Context
import com.example.leonardoarmero.booker.model.entities.Book
import com.example.leonardoarmero.booker.model.persistence.converters.IdentifiersConverter
import com.example.leonardoarmero.booker.model.persistence.converters.StringConverter

@Database(entities = [(Book::class)], version = 1, exportSchema = false)
@TypeConverters(StringConverter::class, IdentifiersConverter::class)
abstract class BooksDatabase : RoomDatabase() {

    abstract fun bookDao(): BookDao

    companion object {
        private var INSTANCE: BooksDatabase? = null
        var callback = object : RoomDatabase.Callback() {
            override fun onOpen(db: SupportSQLiteDatabase) {
                super.onOpen(db)
                print("TODO Init implementation")
            }
        }

        fun getDatabase(context: Context): BooksDatabase {
            if (INSTANCE != null) return INSTANCE!!

            synchronized(BooksDatabase::class) {
                INSTANCE = Room.databaseBuilder(context.applicationContext,
                        BooksDatabase::class.java, "book_database")
                        .fallbackToDestructiveMigration()
                        .addCallback(callback)
                        .build()
            }
            return INSTANCE!!
        }
    }
}