package com.example.leonardoarmero.booker.model.entities

import java.util.*

data class BooksResponse(private val kind: String,
                         private val totalItems: Int,
                         val items: Array<Book>) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as BooksResponse

        if (kind != other.kind) return false
        if (totalItems != other.totalItems) return false
        if (!Arrays.equals(items, other.items)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = kind.hashCode()
        result = 31 * result + totalItems
        result = 31 * result + Arrays.hashCode(items)
        return result
    }
}