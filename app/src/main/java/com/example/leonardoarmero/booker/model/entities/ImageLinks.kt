package com.example.leonardoarmero.booker.model.entities

import android.databinding.BaseObservable
import android.databinding.Bindable
import android.databinding.BindingAdapter
import android.os.Parcel
import android.os.Parcelable
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.example.leonardoarmero.booker.BR

class ImageLinks(val smallThumbnail: String? = null,
                 thumbnail: String?) : BaseObservable(), Parcelable {

    var thumbnail = thumbnail
    @Bindable get
    set(value) {
        field = value
        notifyPropertyChanged(BR.thumbnail)
    }

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(smallThumbnail)
        parcel.writeString(thumbnail)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ImageLinks> {
        override fun createFromParcel(parcel: Parcel): ImageLinks {
            return ImageLinks(parcel)
        }

        override fun newArray(size: Int): Array<ImageLinks?> {
            return arrayOfNulls(size)
        }

        @JvmStatic @BindingAdapter("thumbnail")
        fun loadImage(view: ImageView, thumbnail: String) {
            Glide.with(view.context)
                    .load(thumbnail)
                    .into(view)
        }
    }
}