package com.example.leonardoarmero.booker.model.persistence.converters

import android.arch.persistence.room.TypeConverter
import com.example.leonardoarmero.booker.utils.fromJson
import com.google.gson.Gson

class StringConverter {

    @TypeConverter
    fun stringArrayfromString(value: String?): Array<String>? {
        if (value == null) return null
        val strings = Gson().fromJson<Array<String>>(value)
        return strings
    }

    @TypeConverter
    fun fromArrayString(array: Array<String>?): String? {
        if (array == null) return null

        val gson = Gson()
        val json = gson.toJson(array)
        return json
    }
}