package com.example.leonardoarmero.booker.features.books.adapters

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.util.Pair
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.example.leonardoarmero.booker.R
import com.example.leonardoarmero.booker.features.book.BookActivity
import com.example.leonardoarmero.booker.features.book.BookFragment
import com.example.leonardoarmero.booker.model.entities.Book

class BooksAdapter(private val mContext: Context,
                   private val mTwoPage: Boolean) : RecyclerView.Adapter<BooksAdapter.BooksViewHolder>() {

    private val mInflater = LayoutInflater.from(mContext)
    var books: Array<Book>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BooksViewHolder {
        val view = mInflater.inflate(R.layout.book_item, parent, false)
        return BooksViewHolder(view, mContext, mTwoPage)
    }

    override fun getItemCount() = if (books == null) 0 else books!!.size

    override fun onBindViewHolder(holder: BooksViewHolder, position: Int) {
        if (books == null) return

        holder.setBook(books!![position])
    }

    class BooksViewHolder(view: View,
                          private val mContext: Context,
                          private val mTwoPage: Boolean) : RecyclerView.ViewHolder(view) {
        private val tvTitle = view.findViewById<TextView>(R.id.tvTitle)
        private val tvAuthor = view.findViewById<TextView>(R.id.tvAuthor)
        private val tvPublisher = view.findViewById<TextView>(R.id.tvPublisher)
        private val ivBook = view.findViewById<ImageView>(R.id.ivBook)
        private lateinit var book: Book

        init {
            view.setOnClickListener {
                if (mTwoPage) presentBookFragment()
                else navigateToBook()
            }
        }

        private fun presentBookFragment() {
            val bundle = Bundle()
            bundle.putParcelable(BookActivity.BOOK, book)

            val bookFragment = BookFragment.newInstance(bundle)
            val fragmentManager = (mContext as AppCompatActivity).supportFragmentManager

            if (fragmentManager.fragments.isEmpty()) {
                fragmentManager.beginTransaction()
                        .add(R.id.flContainer, bookFragment)
                        .addToBackStack(null)
                        .commit()
            }
            else {
                (fragmentManager.fragments.first() as BookFragment).book = book
            }
        }

        private fun navigateToBook() {
            val intent = Intent(mContext, BookActivity::class.java)
            intent.putExtra(BookActivity.BOOK, book)
            // Check Android OS version
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                val pair1 = Pair
                        .create<View, String>(ivBook, mContext.getString(R.string.transition_book_image))
                val pair2 = Pair
                        .create<View, String>(tvTitle, mContext.getString(R.string.transition_book_title))
                val options = ActivityOptionsCompat
                        .makeSceneTransitionAnimation(mContext as Activity, pair1, pair2)

                mContext.startActivity(intent, options.toBundle())
            } else {
                mContext.startActivity(intent)
            }
        }

        fun setBook(book: Book) {
            this.book = book
            tvTitle.text = book.volumeInfo?.title
            tvAuthor.text = book.volumeInfo?.authors?.get(0)
            tvPublisher.text = book.volumeInfo?.publisher

            Glide.with(itemView.context)
                    .load(book.volumeInfo?.imageLinks?.thumbnail)
                    .into(ivBook)
        }
    }
}