package com.example.leonardoarmero.booker.model.entities

import android.os.Parcel
import android.os.Parcelable

data class ReadingMode(val text: Boolean?,
                       val image: Boolean?): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean)

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(text)
        parcel.writeValue(image)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ReadingMode> {
        override fun createFromParcel(parcel: Parcel): ReadingMode {
            return ReadingMode(parcel)
        }

        override fun newArray(size: Int): Array<ReadingMode?> {
            return arrayOfNulls(size)
        }
    }
}