package com.example.leonardoarmero.booker.model.entities

import android.arch.persistence.room.Embedded
import android.databinding.BaseObservable
import android.databinding.Bindable
import android.os.Parcel
import android.os.Parcelable
import java.util.*
import com.example.leonardoarmero.booker.BR

class VolumeInfo(title: String?,
                 authors: Array<String>?,
                 publisher: String?,
                 val subtitle: String? = null,
                 val publisherDate: String? = null,
                 val description: String? = null,
                 val industryIdentifiers: Array<IndustryIdentifier>? = null,
                 @Embedded val readingModes: ReadingMode?,
                 val pageCount: Int? = null,
                 val printType: String? = null,
                 val categories: Array<String>? = null,
                 val averageRating: Float? = null,
                 val ratingsCount: Float? = null,
                 val maturityRating: String? = null,
                 val allowAnonLogging: Boolean? = null,
                 val contentVersion: String? = null,
                 @Embedded val panelizationSummary: PenalizationSummary?,
                 imageLinks: ImageLinks?,
                 val language: String? = null,
                 val previewLink: String? = null,
                 val infoLink: String? = null,
                 val canonicalVolumeLink: String? = null) : BaseObservable(), Parcelable {

    var title = title
        @Bindable get
        set(value) {
            field = value
            notifyPropertyChanged(BR.title)
        }

    var authors = authors
        @Bindable get
        set(value) {
            field = value
            notifyPropertyChanged(BR.authors)
        }

    var publisher = publisher
        @Bindable get
        set(value) {
            field = value
            notifyPropertyChanged(BR.publisher)
        }

    @Embedded
    var imageLinks = imageLinks
        @Bindable get
        set(value) {
            field = value
            notifyPropertyChanged(BR.imageLinks)
        }

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.createStringArray(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.createTypedArray(IndustryIdentifier),
            parcel.readParcelable(ReadingMode::class.java.classLoader),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.createStringArray(),
            parcel.readValue(Float::class.java.classLoader) as? Float,
            parcel.readValue(Float::class.java.classLoader) as? Float,
            parcel.readString(),
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
            parcel.readString(),
            parcel.readParcelable(PenalizationSummary::class.java.classLoader),
            parcel.readParcelable(ImageLinks::class.java.classLoader),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(title)
        parcel.writeStringArray(authors)
        parcel.writeString(publisher)
        parcel.writeString(subtitle)
        parcel.writeString(publisherDate)
        parcel.writeString(description)
        parcel.writeTypedArray(industryIdentifiers, flags)
        parcel.writeParcelable(readingModes, flags)
        parcel.writeValue(pageCount)
        parcel.writeString(printType)
        parcel.writeStringArray(categories)
        parcel.writeValue(averageRating)
        parcel.writeValue(ratingsCount)
        parcel.writeString(maturityRating)
        parcel.writeValue(allowAnonLogging)
        parcel.writeString(contentVersion)
        parcel.writeParcelable(panelizationSummary, flags)
        parcel.writeParcelable(imageLinks, flags)
        parcel.writeString(language)
        parcel.writeString(previewLink)
        parcel.writeString(infoLink)
        parcel.writeString(canonicalVolumeLink)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as VolumeInfo

        if (title != other.title) return false
        if (!Arrays.equals(authors, other.authors)) return false
        if (publisher != other.publisher) return false
        if (publisherDate != other.publisherDate) return false

        return true
    }

    override fun hashCode(): Int {
        var result = title?.hashCode() ?: 0
        result = 31 * result + (authors?.let { Arrays.hashCode(it) } ?: 0)
        result = 31 * result + (publisher?.hashCode() ?: 0)
        result = 31 * result + (publisherDate?.hashCode() ?: 0)
        return result
    }

    companion object CREATOR : Parcelable.Creator<VolumeInfo> {
        override fun createFromParcel(parcel: Parcel): VolumeInfo {
            return VolumeInfo(parcel)
        }

        override fun newArray(size: Int): Array<VolumeInfo?> {
            return arrayOfNulls(size)
        }
    }
}