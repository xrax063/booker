package com.example.leonardoarmero.booker.features.books.modules

import android.arch.lifecycle.MutableLiveData
import com.example.leonardoarmero.booker.model.entities.Book
import com.example.leonardoarmero.booker.model.repositories.BooksRepository

import dagger.Component
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module class BooksModule {

    @Provides
    @Singleton
    fun provideEmptyBooksLive(): MutableLiveData<Array<Book>> {
        return MutableLiveData()
    }
}

@Singleton
@Component(modules = [(BooksModule::class)])
interface BooksComponent {
    fun inject(repository: BooksRepository)
}