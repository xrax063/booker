package com.example.leonardoarmero.booker.model.entities

import android.os.Parcel
import android.os.Parcelable

data class PenalizationSummary(val containsEpubBubbles: Boolean?,
                               val containsImageBubbles: Boolean?): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean)

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(containsEpubBubbles)
        parcel.writeValue(containsImageBubbles)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PenalizationSummary> {
        override fun createFromParcel(parcel: Parcel): PenalizationSummary {
            return PenalizationSummary(parcel)
        }

        override fun newArray(size: Int): Array<PenalizationSummary?> {
            return arrayOfNulls(size)
        }
    }
}