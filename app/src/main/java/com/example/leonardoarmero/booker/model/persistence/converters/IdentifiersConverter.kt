package com.example.leonardoarmero.booker.model.persistence.converters

import android.arch.persistence.room.TypeConverter
import com.example.leonardoarmero.booker.model.entities.IndustryIdentifier
import com.example.leonardoarmero.booker.utils.fromJson
import com.google.gson.Gson

class IdentifiersConverter {

    private val gson = Gson()

    @TypeConverter
    fun industryIdentifiersFromString(value: String?): Array<IndustryIdentifier>? {
        return gson.fromJson<Array<IndustryIdentifier>>(value)
    }

    @TypeConverter
    fun stringFromIndustryIdentifiers(identifiers: Array<IndustryIdentifier>?): String? {
        val json = gson.toJson(identifiers)
        return json
    }

}