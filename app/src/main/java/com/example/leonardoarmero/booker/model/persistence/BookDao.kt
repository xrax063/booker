package com.example.leonardoarmero.booker.model.persistence

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.example.leonardoarmero.booker.model.entities.Book

@Dao
interface BookDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(books: Array<out Book?>)

    @Query("DELETE FROM bookTable")
    fun deleteAll()

    @Query("SELECT * FROM bookTable WHERE title LIKE :title ORDER BY title ASC")
    fun getBooks(title: String) : Array<Book>

    @Query("SELECT * FROM bookTable")
    fun getAllBooks(): Array<Book>
}