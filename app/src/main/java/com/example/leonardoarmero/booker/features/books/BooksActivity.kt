package com.example.leonardoarmero.booker.features.books

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.util.EventLog
import android.view.View
import com.example.leonardoarmero.booker.R
import com.example.leonardoarmero.booker.databinding.ActivityBooksBinding
import com.example.leonardoarmero.booker.databinding.BookItemBinding
import com.example.leonardoarmero.booker.features.books.adapters.BooksAdapterBinding
import kotlinx.android.synthetic.main.books_list.*

class BooksActivity : AppCompatActivity() {

    private lateinit var mBinding: ActivityBooksBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setBinding()
        setRvBooks()
    }

    private fun setBinding() {
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_books)

        setSupportActionBar(mBinding.toolbar)
        supportActionBar?.title = title
        supportActionBar?.setDisplayHomeAsUpEnabled(false)

        mBinding.viewModel = ViewModelProviders.of(this).get(BooksViewModel::class.java)
        mBinding.eventHandlers = EventHandlers(supportFragmentManager, this, mBinding)
        mBinding.setLifecycleOwner(this)
    }

    private fun setRvBooks() {
        rvBooks.layoutManager = LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false)

//        val booksAdapter = BooksAdapter(this, flContainer != null)
        val booksAdapter = BooksAdapterBinding(mBinding.eventHandlers!!)

        rvBooks.adapter = booksAdapter

        mBinding.viewModel?.mBooks?.observe(this, Observer {
            booksAdapter.books = it
        })

        mBinding.content.svBookFinder.setOnQueryTextListener(mBinding.eventHandlers?.svQueryTextListener)
    }

    override fun onDestroy() {
        mBinding.viewModel?.quit()
        super.onDestroy()
    }

    /** Events **/
    class EventHandlers(private val fragmentManager: FragmentManager,
                        private val context: Context,
                        private val mBinding: ActivityBooksBinding) {
        private val viewModel = mBinding.viewModel

        val svQueryTextListener = object: SearchView.OnQueryTextListener {
            override fun onQueryTextChange(query: String?): Boolean {
                if (query == null || query.isEmpty()) {
                    viewModel?.clearResults()
                    return false
                }

                viewModel?.searchBooks(query)

                return true
            }

            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query == null || query.isEmpty()) {
                    viewModel?.clearResults()
                    return false
                }

                viewModel?.searchBooks(query)
                return true
            }
        }

        fun onBookClicked(bookBinding: BookItemBinding) {
            if (mBinding.flContainer != null) {
                viewModel?.presentBookFragment(bookBinding.book!!, fragmentManager)
            }
            else viewModel?.navigateToBook(bookBinding, context)
        }

        fun onClearClick(view: View) {
            mBinding.content.svBookFinder.setQuery("", false)
            mBinding.content.svBookFinder.clearFocus()
            viewModel?.deleteAllBooks()
        }
    }
}
