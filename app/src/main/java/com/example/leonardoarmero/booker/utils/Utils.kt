package com.example.leonardoarmero.booker.utils

import android.content.Context
import android.net.ConnectivityManager

class Utils {
    companion object {
        fun isInternetAvailable(context: Context): Boolean {
            val connectionManager = context.getSystemService(Context.CONNECTIVITY_SERVICE)
                    as ConnectivityManager
            val networkInfo = connectionManager.activeNetworkInfo

            return networkInfo != null && networkInfo.isAvailable && networkInfo.isConnected
        }
    }
}