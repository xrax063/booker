package com.example.leonardoarmero.booker.features.application

import android.app.Application
import com.example.leonardoarmero.booker.features.books.modules.BooksComponent
import com.example.leonardoarmero.booker.features.books.modules.BooksModule
import com.example.leonardoarmero.booker.features.books.modules.DaggerBooksComponent

class MainApp: Application() {
    companion object {
        val booksComponent: BooksComponent by lazy {
            DaggerBooksComponent
                    .builder()
                    .booksModule(BooksModule())
                    .build()
        }
    }
}