package com.example.leonardoarmero.booker.features.book

import android.content.Context
import android.support.v4.text.util.LinkifyCompat
import android.support.v7.widget.RecyclerView
import android.text.util.Linkify
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.URLUtil
import android.widget.TextView
import com.example.leonardoarmero.booker.R
import com.example.leonardoarmero.booker.model.entities.Book
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.google.gson.JsonParser

class BookAdapter(book: Book, context: Context) : RecyclerView.Adapter<BookAdapter.BookViewHolder>() {
    private val mInflater = LayoutInflater.from(context)
    private var mPairs = ArrayList<Pair<String, String>>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    private val gson = Gson()
    private val jsonParser = JsonParser()
    var book = book
        set(value) { updateBook(value) }

    init { updateBook(book) }

    private fun updatePairs(json: JsonObject): ArrayList<Pair<String, String>> {
        val pairs: ArrayList<Pair<String, String>> = ArrayList(json.size())
        for (item in json.entrySet()) {

            when(item.value) {
                is JsonObject -> pairs.addAll(updatePairs(item.value as JsonObject))

                is JsonArray -> {
                    var string = ""
                    for (element in item.value as JsonArray) {
                        string += "- ${element.toString().replace("\"", "")} \n"
                    }
                    pairs.add(item.key to string)
                }

                else -> pairs.add(item.key to item.value.toString().replace("\"", ""))
            }
        }

        return pairs
    }

    private fun updateBook(book: Book) {
        val json = jsonParser.parse(gson.toJson(book)).asJsonObject
        mPairs = updatePairs(json)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookViewHolder {
        val view = mInflater.inflate(R.layout.detail_item, parent, false)
        return BookViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mPairs.size
    }

    override fun onBindViewHolder(holder: BookViewHolder, position: Int) {
        val pair = mPairs[position]
        holder.setItem(pair.first, pair.second)
    }

    class BookViewHolder(view: View): RecyclerView.ViewHolder(view) {
        private val tvItem = view.findViewById<TextView>(R.id.tvItem)
        private val tvValue = view.findViewById<TextView>(R.id.tvValue)

        fun setItem(name: String, value: String) {
            tvItem.text = name
            tvValue.text = value

            if (URLUtil.isValidUrl(value)) LinkifyCompat.addLinks(tvValue, Linkify.WEB_URLS)
        }
    }
}