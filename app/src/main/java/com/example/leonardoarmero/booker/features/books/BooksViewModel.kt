package com.example.leonardoarmero.booker.features.books

import android.app.Activity
import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.app.FragmentManager
import android.support.v4.util.Pair
import android.support.v7.widget.SearchView
import android.view.View
import com.example.leonardoarmero.booker.R
import com.example.leonardoarmero.booker.databinding.BookItemBinding
import com.example.leonardoarmero.booker.databinding.BooksListBinding
import com.example.leonardoarmero.booker.features.book.BookActivity
import com.example.leonardoarmero.booker.features.book.BookFragment
import com.example.leonardoarmero.booker.model.entities.Book
import com.example.leonardoarmero.booker.model.repositories.BooksRepository


class BooksViewModel(application: Application):
        AndroidViewModel(application) {

    private var mRepository = BooksRepository(application)
    var mBooks = mRepository.books

    fun deleteAllBooks() { mRepository.deleteAllBooks() }

    fun presentBookFragment(book: Book, fragmentManager: FragmentManager) {
        val bundle = Bundle()
        bundle.putParcelable(BookActivity.BOOK, book)

        val bookFragment = BookFragment.newInstance(bundle)

        if (fragmentManager.fragments.isEmpty()) {
            fragmentManager.beginTransaction()
                    .add(R.id.flContainer, bookFragment)
                    .addToBackStack(null)
                    .commit()
        }
        else {
            (fragmentManager.fragments.first() as BookFragment).book = book
        }
    }

    fun navigateToBook(bookBinding: BookItemBinding, context: Context) {
        val intent = Intent(context, BookActivity::class.java)
        intent.putExtra(BookActivity.BOOK, bookBinding.book!!)

        // Check Android OS version
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val pair1 = Pair
                    .create<View, String>(bookBinding.ivBook, context.getString(R.string.transition_book_image))
            val pair2 = Pair
                    .create<View, String>(bookBinding.tvTitle, context.getString(R.string.transition_book_title))
            val options = ActivityOptionsCompat
                    .makeSceneTransitionAnimation(context as Activity, pair1, pair2)

            context.startActivity(intent, options.toBundle())
        } else {
            context.startActivity(intent)
        }
    }

    fun clearResults() { mRepository.clearResults() }

    fun searchBooks(query: String) {
        mRepository.searchBooks(query)
    }

    fun quit() { mRepository.quit() }
}