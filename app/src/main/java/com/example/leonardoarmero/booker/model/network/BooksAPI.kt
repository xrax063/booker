package com.example.leonardoarmero.booker.model.network

import com.example.leonardoarmero.booker.model.entities.BooksResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface BooksAPI {

    @GET("books/v1/volumes")
    fun getBooks(@Query("q") query: String,
                 @Query("maxResults") maxResults: Int = 10,
                 @Query("printType") printType: String = "books")
            : Call<BooksResponse>
}