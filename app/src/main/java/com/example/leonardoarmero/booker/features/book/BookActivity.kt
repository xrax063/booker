package com.example.leonardoarmero.booker.features.book

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.example.leonardoarmero.booker.R
import com.example.leonardoarmero.booker.databinding.ActivityBookBinding

class BookActivity : AppCompatActivity() {

    private lateinit var mBinding: ActivityBookBinding

    companion object {
        const val BOOK = "BOOK"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setBinding()

        if (savedInstanceState == null) {
            val bookFragment = BookFragment.newInstance(intent.extras)
            supportFragmentManager
                    .beginTransaction()
                    .add(R.id.flContainer, bookFragment)
                    .commit()
        }
    }

    private fun setBinding() {
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_book)
        setSupportActionBar(mBinding.toolbar)

        supportActionBar?.title = title
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> onBackPressed()
        }

        return true
    }
}
