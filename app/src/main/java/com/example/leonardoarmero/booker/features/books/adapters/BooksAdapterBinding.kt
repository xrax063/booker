package com.example.leonardoarmero.booker.features.books.adapters

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.leonardoarmero.booker.R
import com.example.leonardoarmero.booker.databinding.BookItemBinding
import com.example.leonardoarmero.booker.features.books.BooksActivity
import com.example.leonardoarmero.booker.features.books.BooksViewModel
import com.example.leonardoarmero.booker.model.entities.Book

class BooksAdapterBinding(private val eventHandlers: BooksActivity.EventHandlers):
    RecyclerView.Adapter<BooksAdapterBinding.BooksViewHolderBinding>() {

    var books: Array<Book>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    private var mLayoutInflater: LayoutInflater? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BooksViewHolderBinding {
        if (mLayoutInflater == null) mLayoutInflater = LayoutInflater.from(parent.context)

        val binding = DataBindingUtil.inflate<BookItemBinding>(mLayoutInflater!!,
                R.layout.book_item, parent, false)
        return BooksViewHolderBinding(binding)
    }

    override fun getItemCount() = if (books == null) 0 else books!!.size

    override fun onBindViewHolder(holder: BooksViewHolderBinding, position: Int) {
        holder.binding.book = books?.get(position)
        holder.binding.root.setOnClickListener {
            eventHandlers.onBookClicked(holder.binding)
        }
    }

    class BooksViewHolderBinding(val binding: BookItemBinding):
            RecyclerView.ViewHolder(binding.root)
}