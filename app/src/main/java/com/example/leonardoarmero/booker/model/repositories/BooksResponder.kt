package com.example.leonardoarmero.booker.model.repositories

import com.example.leonardoarmero.booker.model.entities.Book

interface BooksResponder {
    fun onBooksReceived(books: Array<Book>?)
}