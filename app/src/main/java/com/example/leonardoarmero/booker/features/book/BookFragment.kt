package com.example.leonardoarmero.booker.features.book

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.leonardoarmero.booker.R
import com.example.leonardoarmero.booker.databinding.FragmentBookBinding
import com.example.leonardoarmero.booker.model.entities.Book

class BookFragment : Fragment() {

    var book: Book? = null
        set(value) {
            if(value != null) bookUpdated(value)
        }

    private lateinit var mBinding: FragmentBookBinding

    companion object {
        fun newInstance(bundle: Bundle): BookFragment {
            val bookFragment = BookFragment()
            bookFragment.arguments = bundle
            return bookFragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_book,
                container, false)
        retainInstance = true

        mBinding.rvItems.layoutManager = LinearLayoutManager(context,
                LinearLayoutManager.VERTICAL, false)

        book = arguments?.get(BookActivity.BOOK) as Book

        return mBinding.root
    }

    private fun bookUpdated(book: Book) {
        mBinding.book = book

        if (context != null) {
            mBinding.rvItems.adapter = BookAdapter(book, context!!)
        }
    }
}