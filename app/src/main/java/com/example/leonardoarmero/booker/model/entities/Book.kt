package com.example.leonardoarmero.booker.model.entities

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.databinding.BaseObservable
import android.databinding.Bindable
import android.os.Parcel
import android.os.Parcelable
import com.example.leonardoarmero.booker.BR
import org.jetbrains.annotations.NotNull

@Entity(tableName = "bookTable")
class Book(id: String,
           selfLink: String?,
           volumeInfo: VolumeInfo?) : BaseObservable(), Parcelable {

    @PrimaryKey
    @NotNull
    var id = id
        @Bindable get
        set(value) {
            field = value
            notifyPropertyChanged(BR.id)
        }

    var selfLink = selfLink
    @Bindable get
    set(value) {
        field = value
        notifyPropertyChanged(BR.selfLink)
    }

    @Embedded
    var volumeInfo = volumeInfo
        @Bindable get
        set(value) {
            field = value
            notifyPropertyChanged(BR.volumeInfo)
        }

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readParcelable(VolumeInfo::class.java.classLoader))

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(selfLink)
        parcel.writeParcelable(volumeInfo, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Book> {
        override fun createFromParcel(parcel: Parcel): Book {
            return Book(parcel)
        }

        override fun newArray(size: Int): Array<Book?> {
            return arrayOfNulls(size)
        }
    }
}